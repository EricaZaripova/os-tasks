import re
import copy


class MoveToFront:
    def __init__(self, data, alphabet):
        self.data = []
        self.alphabet = alphabet
        self.alphabet_ = copy.deepcopy(alphabet)
        self.length = len(data)
        self.word = data
        self.dec = ""

    def create_decode(self):
        count = 0
        while 2 ** count < len(self.alphabet):
            count += 1
        chunks = re.findall('.{%s}' % count, self.word)
        for chunk in chunks:
            self.data.append(int(chunk, 2))
        self.length = len(self.data)
        for i in range(self.length):
            now = self.alphabet_[int(self.data[i])]
            self.dec += now
            self.alphabet_.insert(0, self.alphabet_[int(self.data[i])])
            self.alphabet_.pop(int(self.data[i])+1)
