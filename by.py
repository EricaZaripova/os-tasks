class BurrowsWheeler:
    def __init__(self, data, length, line):
        self.data = data
        self.line = line
        self.length = length
        self.result = ""
        self.word = ""

    def ordering(self):
        self.line = int(f"0b{self.line}", 2)
        g = [list(self.data) for _ in range(self.length)]
        c = [[] for _ in range(self.length)]
        if self.length > 1:
            for i in range(1, self.length):
                c[i] = sorted(g[i-1])
                for j in range(self.length):
                    g[i][j] += c[i][j]
            result_dict = sorted(g[-1])
            self.result = result_dict[self.line]
        else:
            self.result = self.data
