from movetofront_decoding import MoveToFront
from arithmetic_coding import Arithmetic
from by import BurrowsWheeler


def read_alg(file_name):
    with open(file_name, "r") as f:
        lines = f.readlines()
        p = []
        alphabet = lines[0].split()
        try:
            line = lines[1].split()
            for element in line:
                h = element.split("/")
                p.append(int(h[0])/int(h[1]))
                # p[i] = int(p[i].replace(",", "."))
        except Exception:
            p = ""
        return [alphabet, p]


def create_frequency_table(alp):
    frequency_dict = {}
    for i in range(len(alp[0])):
        frequency_dict[alp[0][i]] = alp[1][i]
    return frequency_dict


def validate(data, length, alphabet):
    for i in range(length):
        if data[i] not in alphabet:
            quit("В слове есть лишний символ")


def coder():
    alphabet = read_alg("input_file_alg")
    alphabet = create_frequency_table(alp=alphabet)
    data = input(" Введите слово: ")
    code = Arithmetic(frequency_table=alphabet)
    encoder, encoded_msg = code.encode(msg=data, probability_table=code.probability_table)
    print("Ваш код:", encoded_msg)


def decoder():
    alphabet = read_alg("input_file_mv")
    data = input("Введите код: ")
    line = input('Введите номер строки: ')

    code = MoveToFront(data=data, alphabet=alphabet[0])
    code.create_decode()
    data = code.dec

    by_ = BurrowsWheeler(data=data, length=len(data), line=line)
    by_.ordering()

    print('Ваше слово: ', by_.result)


if __name__ == '__main__':
    c = int(input('Введите 1, если хотите закодировать сообщение арифметическим кодированием\n'
                  'Введите 2, если хотите раскодировать сообщение методом стопка книг+Б/У\n'))

    if c == 1:
        coder()
    elif c == 2:
        decoder()
    else:
        print('Неверный ввод')
